# intro-compsci-class

Material developed for [Texas A&M University - Commerce](http://tamuc.edu) course CSci 509: Introduction to Computational Science.  These materials were initially developed in the Fall of 2013.  Many of the notebooks were originally created by Robert Johansson and you can find the original repository at  [Lectures on scientific computing with Python](https://github.com/jrjohansson/scientific-python-lectures#online-read-only-versions)

## Content

- The interactive notebooks are in the lectures folder.

- All assignments for the course will appear in the assignments folder and/or will be posted through our universities online learning system (MyLeoOnline).

## More content

Course Textbook:

- Langtangen (2012).  [A Primer on Scientific Programming with Python](http://www.amazon.com/Scientific-Programming-Computational-Science-Engineering/dp/3642302920/ref=sr_1_1?ie=UTF8&qid=1391711216&sr=8-1&keywords=langtangen+primer+on+scientific+computing) 3ed.  Springer.
- Newman (2012). [Computational Physics](http://www.amazon.com/Computational-Physics-Mark-Newman/dp/1480145513/ref=sr_1_1?ie=UTF8&qid=1423234928&sr=8-1&keywords=newman+computational+physics) CreateSpace Independent Publishing.

Suggested material for learning python:

- [Think Python: How to think like a computer scientist](https://greenteapress.com/wp/think-python-2e/) free online textbook, very good resource for not only Python but learning to program in general.
- [Google Developers Python Class](https://developers.google.com/edu/python/?hl=ru&csw=1) short course with videos, might be helpful for those looking for video tutorials of Python.
- [Software Carpentry](http://swcarpentry.github.io/python-novice-inflammation/) section on learning Python is also very good, and also includes videos.

Companion courses / repositories:

- I will be borrowing material from this course repository: [Lectures on scientific computing with Python](https://github.com/jrjohansson/scientific-python-lectures#online-read-only-versions)


# Getting Started

Before the end of the first week of class, you need to get a working Python distribution installed on your personal machine, and you need to clone a copy of our class repository.  The following video should help you in getting started: [Getting Started](http://derekharter.com/class/videos/jupyter-git-setup-250.webm)

In order to do the class lectures and readings, you need to be able to run and execute Jupyter notebooks.  In general, you need to complete the following 4 steps.

1. Download and install a Python Distribution, such as Anaconda, that includes support for Jupyter notebooks.
2. Download and install a git client on your machine.
3. Clone the class repository.
4. Test out Python, running Jupyter notebooks, and that you can access and execute the course lecture notebooks with your system setup.

## Download and Install a Python Distribution

For this course we recommend using a Python scientific distribution. We recommend using the Anaconda distribution, though the Enthought Canopy distribution should be fine as well.

- [Anaconda Distribution Download](https://www.anaconda.com/download/)
- [Enthought Canopy Distribution Download](https://store.enthought.com/downloads/)

Whether you are using Windows, Mac or Linux, the linked to installers should work for you.  We are using Python version 3.7 for this class, so please download and install the 3.7 version of the installer.

## Download and Install Git Client

For Linux or Mac users, if git is not already installed you can probably most easily use the standard package management systems of your OS to install git.  For Windows, or to install it by hand on Linux/Mac, you should get the package from the SCM git site:

- [Git download package](https://git-scm.com/downloads)

## Clone Class Repository

The class repository for our Introduction to Computational Science class can be found at: https://bitbucket.org/dharter/intro-compsci-class

To clone the repository from a dos terminal or command line prompt, once git is installed, do the following

    $ git clone https://bitbucket.org/dharter/intro-compsci-class.git


## Test Python, Jupyter and Class Notebooks

There are multiple ways to start up a Jupyter notebook server on your system once you have Python and Jupyer installed.  From a dos prompt or the command line, first change to the directory where you cloned your class repository into, and then execute the command

    $ jupyter notebook

This will start up a notebook server, and on most systems will open up a file browser inside of your default web browser, in order for you to browse and select iPython notebooks for execution.
