{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tools for Working Scientists, Computational or Otherwise"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this lecture we will discuss and introduce you to a range of other tools and systems that are used by large numbers of scientists working on computational modeling problems, and just plain old scientific research.  This overview will not be exhaustive.  We will give links and examples for further exploration of where you can find resources to learn more and pick up some of these skills.  As a working programmer, software engineer, or scientist, we must always be trying to expand our set of tools and skills to help us be more productive  and rigorous.  As mentioned in the first lecture, some of the particular goals of a scientist are:\n",
    "\n",
    "1. Produce reproducible results (replication)\n",
    "2. Produce correct results (rigour)\n",
    "3. Disseminate our results, through peer review and education (review)\n",
    "\n",
    "I guess you could call these the 3 r's of Science in general.\n",
    "\n",
    "All of these goals can be enhanced through the proper application of tools.  As an experimental researcher will use a lab notebook, and develop strict protocols for doing experimentation, so must a computational modeler use proper tools in order to produce good science."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unix and the *NIX Command Line Ecosystem\n",
    "----------------------------------------\n",
    "\n",
    "- Automation\n",
    "- Productivity\n",
    "- Text PreProcessing\n",
    "- Automation through macro like shell scripting.\n",
    "- Did I mention automation?\n",
    "- MacOS are actually already Unix based, can use the full suite of unix command line ecosystem tools\n",
    "- Or linux, free full Unix-like, very easy to install and use these days.\n",
    "\n",
    "Unix, whether you realize it or not, is arguably the most successful general purpose computing system OS design that has been created.  There are many reasons for this, most beyond the scope of this course.  But there is a reason that the overwhelming majority of research scientists and engineers doing computational software development are doing it in Unix environments.  \n",
    "\n",
    "Many operating systems touted as more ‘modern’ or ‘user friendly’ than Unix achieve their surface glossiness by locking users and developers into one interface policy, and offer an application-programming interface that for all its elaborateness is rather narrow and rigid. On such systems, tasks the designers have anticipated are very easy — but tasks they have not anticipated are often impossible or at best extremely painful.\n",
    "\n",
    "Unix, on the other hand, has flexibility in depth. The many ways Unix provides to glue together programs mean that components of its basic toolkit can be combined to produce useful effects that the designers of the individual toolkit parts never anticipated.\n",
    "\n",
    "Unix's support of multiple styles of program interface (often seen as a weakness because it increases the perceived complexity of the system to end users) also contributes to flexibility; no program that wants to be a simple piece of data plumbing is forced to carry the complexity overhead of an elaborate GUI.\n",
    "\n",
    "Unix tradition lays heavy emphasis on keeping programming interfaces relatively small, clean, and orthogonal — another trait that produces flexibility in depth. Throughout a Unix system, easy things are easy and hard things are at least possible.\n",
    "\n",
    "I find Unix essential for my everyday workflow.  The bash command line environment allows for a programmable, scripted workflow environment, that allows me to automate many tasks that I see others performing laborously by hand.  The philosophy of small programs glued together to create larger solutions permeates the Unix ecosystem. There is more complexity, thus a steaper learning curve in order to get started, but the payoff in terms of increased productivity is hard to describe.  And now with freely available versions of Unix like systems available as Linux distributions, there is no real reason you can't get a Unix system for your own personal hardware and learn/try them out yourself.  I would recommend starting with the [Ubuntu](http://www.ubuntu.com/download/desktop) distribution.  Now adays, much of the pain of installing Unix on a wide range of hardware has been eliminated.  I recently did several installs of Windows 8 and Ubuntu 13.04, and I would say that the Ubuntu 13.04 installs are much easier and less likely to run into problems (and no need for licenses or to pay anyone, yea!).\n",
    "\n",
    "**References:**\n",
    "\n",
    "* [The Art of Unix Programming](http://catb.org/esr/writings/taoup/)\n",
    "* [Ubuntu Linux](http://www.ubuntu.com/download/desktop)\n",
    "* [Unix as IDE](http://blog.sanctum.geek.nz/series/unix-as-ide/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\LaTeX~~$ Document Processing\n",
    "-------------------------\n",
    "\n",
    "- Document markup language\n",
    "- Mathematical and technical formula, markup has become the standard for expressing these\n",
    "- LaTeX markup used in iPython notebooks for expresing mathematical notation\n",
    "- Produces professional quality typsetting and graphics layout, suitable for journals, technical reports, thesis, etc. \n",
    "\n",
    "$\\LaTeX~$ is a document preparation system for high-quality typesetting. It is most often used for medium-to-large technical or scientific documents but it can be used for almost any form of publishing.\n",
    "\n",
    "$\\LaTeX~$ is not a word processor! Instead, $\\LaTeX~$ encourages authors not to worry too much about the appearance of their documents but to concentrate on getting the right content.  $\\LaTeX~$ is really a document markup language, like HTML, in which you describe the document structure, and some other process renders that into the final format, whether that be a web page, a PDF document, or whatever.\n",
    "\n",
    "LaTeX contains features for:\n",
    "\n",
    "- Typesetting journal articles, technical reports, books, and slide presentations.\n",
    "- Control over large documents containing sectioning, cross-references, tables and figures.\n",
    "- Typesetting of complex mathematical formulas.\n",
    "- Advanced typesetting of mathematics with AMS-$\\LaTeX~$.\n",
    "- Automatic generation of bibliographies and indexes.\n",
    "- Multi-lingual typesetting.\n",
    "- Inclusion of artwork, and process or spot colour.\n",
    "- Using PostScript or Metafont fonts.\n",
    "\n",
    "I'll just mention two from this list that are of particular use to me.  If you need to write technical or scientific reports, you will need to justify your thought by referencing prior work.  The automatic genreation of bibliographies is a **huge** timesaver for such technical reports.  I set up a database of papers or work I wish to reference.  Then add a simple markup at the appropriate location in my document to make a reference.  $\\LaTeX~$ takes care of formating the reference and bibliography in the appropriate style that you specify.  Also scientific work relies heavily on precisly formalizing your descriptions as mathematical notations.  $\\LaTeX~$ supports formatting of advanced mathematical formula in documentation, and is the de facto standard for specifying such equations.\n",
    "\n",
    "And, as an added bonus, iPython can understand and render $\\LaTeX~$ mathematical expressions in its markup:\n",
    "\n",
    "$\n",
    "A_{m,n} =\n",
    " \\begin{pmatrix}\n",
    "  a_{1,1} & a_{1,2} & \\cdots & a_{1,n} \\\\\\\\\n",
    "  a_{2,1} & a_{2,2} & \\cdots & a_{2,n} \\\\\\\\\n",
    "  \\vdots  & \\vdots  & \\ddots & \\vdots  \\\\\\\\\n",
    "  a_{m,1} & a_{m,2} & \\cdots & a_{m,n}\n",
    " \\end{pmatrix}\n",
    "$\n",
    "\n",
    "$\n",
    "\\frac{n!}{k!(n-k)!} = \\binom{n}{k}\n",
    "$\n",
    "\n",
    "**References:**\n",
    "\n",
    "* [An Introduction to $ \\LaTeX~$](http://latex-project.org/intro.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Programming/Plain Text Editor\n",
    "-----------------------------\n",
    "\n",
    "- Should learn a good one that supports multiple languages\n",
    "- with syntax highlighting\n",
    "- built in help\n",
    "- code completion\n",
    "- Ties to automated build and unit test execution\n",
    "\n",
    "Text is the raw material of the craft of programming. You should become good at manipulating text. The first thing you should do is pick a good cross-platform text editor and learn it well. If you haven't already done so, this guide will help you choose one. The next thing you should do is learn regular expressions, and the next thing after that is learn UNIX and one of its command shells, probably bash.\n",
    "\n",
    "I use Emacs, but it has a steep learning curve. If you want something you'll be productive with quickly try Sublime Text 2 or jEdit. Many people who insist on using Windows like Notepad++. But if you're going to get a computer science or engineering degree, you might as well learn Emacs or Vim, the two major editors available on every UNIX platform, Windows, and Mac. Vim is probably easier. Another advantage of learning these is some of the basic keybindings of these are used in other contexts in Unix, thus you become familiar with using keyboard shortcuts for other tools when you learn these.  Even if you don't pick one of these, it really is important to have some programming text editor that you know fairly well and that you can be productive with.  Just a little job search hint, one way you can differentiate an actual  software developer who is serious about his/her craft is ascertaining what programming editor they use, and how proficient they are at using it.  I often like to just ask people I'm interviewing to bring their editor of choice, and write a simple program in a language of their choosing using the editor.  You can learn a lot about a person's proficiency and skill level from this simple session.\n",
    "\n",
    "But back to an editor.  I listed some of these in the bullet points but, a good modern text editor at a minimum can support multiple languages, will at a minimum do automatic syntax highlighting and context sensitive help and code completion.  Advanced features to look for are built in hooks to automation tools, such as unit testing, system building, etc.  Having a go-to text editor improves your productivity in part because it will be set up with your preferred defaults. Customization is important because many text editors and IDEs are configured with bad defaults. Good programmers insist on writing code that is clear and consistently formatted. Having a well-configured text editor helps you do that\n",
    "\n",
    "**References**:\n",
    "\n",
    "* [Introduction to Text Editors for Young Programmers](http://cs1331.gatech.edu/text-editors.html)\n",
    "* [A Guided Tour of Emacs](http://www.gnu.org/software/emacs/tour/)\n",
    "* [The Interactive Vim Tutorial](http://www.openvim.com/tutorial.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<img src=\"http://imgs.xkcd.com/comics/real_programmers.png\"/>"
      ],
      "text/plain": [
       "<IPython.core.display.Image object>"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from IPython.core.display import Image \n",
    "Image(url='http://imgs.xkcd.com/comics/real_programmers.png') "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Data Analysis and Visuzliation\n",
    "------------------------------\n",
    "\n",
    "- R is defacto standard in statistical data analysis, free and easily obtainable on all platforms.\n",
    "- Though have a look at the pandas Python library, goal of replicating the R DataFrame and whole R ecosystem in Python.  Already good enough for most statistical analysis tasks, and if you already know Python...\n",
    "\n",
    "It is probably still worth it to learn R for data analysis, just as its worth it to learn Matlab (see below), because of the many shops and labs that rely on them.  R is an open soure clone of the S/S-Plus statistical package that was the leader in this field before R.  R / S when first conceived, was revolutionary in providing an integrated system for data analysis, including capabilities that we today associate with scripting languages, together with extensive graphing abilities.  The strongest feature of R is the large number of built-in (user-contributed) functions for primarily statistical calculations.  R is not intended as a general numerical workbench, like Matlab, but is focused on statistical analysis and visuzliation.  A serious problem when working with R is its dated programming model.  It relies heavily on implicit behavior and \"reasonable defaults,\" which leads to particularly hard to understand programs.\n",
    "\n",
    "I have recently begun using the pandas library for doing statistical data analysis in Python.  It is relatively new, and certainly can't yet compete with R in terms of the breadth of features.  Pandas is intended to eventually be as capable as R, and being implemented in python with a new from scratch design, has a much more intuitive interactive model.  Pandas is inspired by R, for example it implements a DataFrame object and adds it to the Python language that works in the same way as an R DataFrame.  Pandas is also being written with performance in mind, and usually performs as good, if not often better than R, a big plus when trying to do exploratory data analysis with really large data sets.  And in terms of the first mentioned defect, it already does contain all of the basic types of statistical analysis and data preprocessing capabilities that most working scientists are likely to need. \n",
    "\n",
    "**References**:\n",
    "\n",
    "* [R Project page](http://www.r-project.org/)\n",
    "* [Pandas library page](http://pandas.pydata.org/)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Reproducible Research and Literate Programming\n",
    "---------------------------------------------\n",
    "\n",
    "- Mentioned before, basic idea is a movement to demand computational models in published scientific research are available for the peer review and as part of the research publication.\n",
    "- Literate programming is related, idea of having artifacts that are a mix of documentation with executable code.  Very good for documenting exact procedures and protocols in computational experiments and statistical data analysis processing pipelines.\n",
    "- org-mode in Emacs\n",
    "- iPython which you are seeing is another.\n",
    "- See videos on tips for reproducible research.\n",
    "\n",
    "In experimental and theoretical sciences there are well established codes of conduct for how results and methods are published and made available to other scientists. For example, in theoretical sciences, derivations, proofs and other results are published in full detail, or made available upon request. Likewise, in experimental sciences, the methods used and the results are published, and all experimental data should be available upon request. It is considered unscientific to withhold crucial details in a theoretical proof or experimental method, that would hinder other scientists from replicating and reproducing the results.\n",
    "\n",
    "In computational sciences there are not yet any well established guidelines for how source code and generated data should be handled. For example, it is relatively rare that source code used in simulations for published papers are provided to readers, in contrast to the open nature of experimental and theoretical work. And it is not uncommon that source code for simulation software is withheld and considered a competitive advantage (or unnecessary to publish).\n",
    "\n",
    "However, this issue has recently started to attract increasing attention, and a number of editorials in high-profile journals have called for increased openness in computational sciences. Some prestigious journals, including Science, have even started to demand of authors to provide the source code for simulation software used in publications to readers upon request.\n",
    "Discussions are also ongoing on how to facilitate distribution of scientific software, for example as supplementary materials to scientific papers.\n",
    "\n",
    "**References**:\n",
    "\n",
    " * [Reproducible Research in Computational Science](http://dx.doi.org/10.1126/science.1213847), Roger D. Peng, Science 334, 1226 (2011).\n",
    " * [Shining Light into Black Boxes](http://dx.doi.org/10.1126/science.1218263), A. Morin et al., Science 336, 159-160 (2012).\n",
    " * [The case for open computer programs](http://dx.doi.org/doi:10.1038/nature10836), D.C. Ince, Nature 482, 485 (2012).\n",
    " * [An Efficient Workflow for Reproducible Science](http://www.youtube.com/watch?v=Y-XFNg0QS14) Presentaion by Trevor Bekolay from University of Waterloo at the 2013 SciPy conference.\n",
    " * [Emacs + org-mode + python in Reproducible Research](http://www.youtube.com/watch?v=1-dUkyn_fZA) Presentation by John Kitchin from Carnegie Mellon Universty at the 2013 SciPy conference.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Automation Tools\n",
    "----------------\n",
    "\n",
    "- Makefile\n",
    "- SCONS\n",
    "- ant\n",
    "\n",
    "\n",
    "Mastering a general automation tool that you can use to automatically build products from a set of source files the product depends upon can greatly increase your productivity, as well as help you with your accuracy, reproducibility and creating correct software. This is because you can encapsulate the entire workflow of how to get from raw source data and code to the final products of your research, like research papers, simulations to be run, etc.  Make is a bit dated, but still worth learning.  It was originally invented to manage compilation of programs written in languages like C.  It can be used to automatically update any files that depend on another set of files.  This makes it a good solution for many data analysis and management workflows.\n",
    "\n",
    "There are more powerful alternatives, but none are yet as widely adopted as Make.  Many IDE environments you may use, like Eclipse or VisualStudio actually use some version of Make behind the scenes to automate a project build.  Ant is an equivilent tool built to work with Java projects.  It can work with projects written in different languages, but is still best as a special purpose build automation tool for Java enterprise builds.  There are some interesting attempts to build automation tools based on Python (SCONS), but these are certainly not yet very widely adapted or seen in the wild.\n",
    "\n",
    "**References**:\n",
    "\n",
    "* [Software Carpentry Tutorials on Make](http://swcarpentry.github.io/make-novice/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tools for Managing Source Code\n",
    "------------------------------\n",
    "\n",
    "Ensuring replicability and reprodicibility of scientific simulations is a *complicated problem*, but there are good tools to help with this:\n",
    "\n",
    "* Revision Control System (RCS) software. \n",
    "    * Good choices include:\n",
    "        * git - http://git-scm.com\n",
    "        * mercurial - https://www.mercurial-scm.org. Also known as `hg`.\n",
    "        * subversion - http://subversion.apache.org. Also known as `svn`.\n",
    "\n",
    "* Online repositories for source code. Available as both private and public repositories. \n",
    "    * Some good alternatives are\n",
    "        * Github - http://www.github.com\n",
    "        * Bitbucket - http://www.bitbucket.com\n",
    "        * Privately hosted repositories on the university's or department's servers.\n",
    "\n",
    "#### Note\n",
    "\t\n",
    "Repositories are also excellent for version controlling manuscripts, figures, thesis files, data files, lab logs, etc. Basically for any digital content that must be preserved and is frequently updated. Again, both public and private repositories are readily available. They are also excellent collaboration tools!\n",
    "\n",
    "**References**:\n",
    "\n",
    "* [Official git Tutorial](http://git-scm.com/docs/gittutorial)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Matlab vs. Python / SciPy / NumPy\n",
    "---------------------------------\n",
    "\n",
    "- Programming environments for scientific computation and numerical calculations\n",
    "\n",
    "Matlab has been around since the mid-1980s; it has a very large user base, mostly in engineering shops but also in pure mathematics and in the machine learning community.  Matlab was designed to be used interactively and it implements a general numerical computing environment for vector and matrix based calculations.  Matlab was the first environmet to implement vectors and matrices as built-in data types of the language, and allowing for operations to be performed between them without looping (vector opertions, or vectorization) was revolutionary.  Today, overloading in object oriented languages basically give a similar capability.  Matlab's programming model has serious deficiencies for larger programming projects (the Matlab language itself was created before object oriented concepts became wide spread, and it retains its clunky roots).\n",
    "\n",
    "Python by contrast was developed to be a general purpose scripting language, with interactive use in mind.  Python has become the scripting language of choice for scientists and scientific applications.  Python is easy to learn, and incorporates a clean syntax and programming model, with all of the modern concepts built in from the ground up for programming. Thus Python is both easy to learn and has a low learning curve to become productive, but is still quite suitable for enterprise level programming projects.  With the addition of specialized libraries for numeric, scientific and visuzliation algorithms, Python offers a complete ecosystem that in many ways  exceeds Matlab.   NumPy in particular, adds vector/matrix data types to the core language, to replicate the functionality of Matlab vectors and matrices (with cleaner syntax and a better programming model).  Matplotlib, likewise, adds equivalent visualization and plotting features to the core language to parallel Matlab's capabilities.  \n",
    "\n",
    "Matlab is still the 800-pound gorilla of scientific software.  As a commercially developed product, it has a certain amount of \"polish\" that many open source alternatives lack.  It is thus definitely worth learning and using seriously, if you have funds to afford it.  If you need an open source alternative in order to become familiar with the basics of the Matlab command line environment, you can try the open source octave attempt to clone Matlab.  The Python/NumPy/SciPy/Matplotlib ecosystem, however, is clearly worth learning.  Its adoption and popularity in scientific, engineering and research shops has grown rapidly, and is quickly becoming the de facto standard environment in many different areas.\n",
    "\n",
    "**References**:\n",
    "\n",
    "* Some of the above comparison was taken from [Data Analysis with Open Source Tools](http://www.amazon.com/Data-Analysis-Open-Source-Tools/dp/0596802358) by Janert, O'Reilly, 2012.  See Appendix A."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Software Carpentry\n",
    "------------------\n",
    "\n",
    "I highly recommend this site as another source for more tools and ideas about reproducible research for working computational scientists.\n",
    "\n",
    "**References**:\n",
    "\n",
    "* [Software Carpentry](http://software-carpentry.org/index.html)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
