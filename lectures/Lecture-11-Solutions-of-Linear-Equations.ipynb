{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Simultaneous Linear Equations\n",
    "\n",
    "\n",
    "One of the most common uses of computers in scientific modeling is in the solution of equations or sets of equations.  Solutions\n",
    "of linear equations involve techniques from linear algebra, where we represent the parameters of the equations as sets of\n",
    "matrices and vectors.  NumPy already contains many of the routines we will be discussing here, built in to the library\n",
    "`numpy.linalg` library, including algorithms for solving such linear systems of equations.  Algorithms that \n",
    "are useful include inversion and the\n",
    "diagonalization of matrices.  We will look at building these techniques by hand, so that we can better understand\n",
    "the algorithms underlying these techniques, and better grasp the uses that such solutions and techniques can be\n",
    "put towards.\n",
    "\n",
    "A single linear equation in one variable, such as $x - 1 = 0$ is trivial to solve.  We do not need computers to do\n",
    "this for us.  But simultaneous sets of linear equations in many variables are harder.  In principle the techniques\n",
    "for solving such systems are well understood and straightforward, you should have learned them at some point in\n",
    "school.  But they are tedious, and when we are talking about realistic scientific models that can have thousands\n",
    "or even millions of variables, thy are beyond the ability of a human to effectively compute solutions.  Humans are slow and prone\n",
    "to errors in such calculations, but since the techniques are well defined, they can be described algorithmically and\n",
    "implemented on computers which are perfectly suited to carying out such tedious tasks for us.\n",
    "\n",
    "Lets take an example, to refresh our memories on how such systems of linear equations work.  Suppose we want to\n",
    "solve the following four simultaneous equations for the variables $w$, $x$, $y$ and $z$:\n",
    "\n",
    "$$\n",
    "\\begin{eqnarray}\n",
    "2w + x + 4y + z &=& -4 \\\\\n",
    "3w + 4x - y - z &=& 3 \\\\\n",
    "w - 4x + y + 5z &=& 9 \\\\\n",
    "2w - 2x + y + 3z &=& 7\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "For computational purposes the simplest way to think of these is in matrix form.  They can be written as:\n",
    "\n",
    "$$\n",
    "\\begin{pmatrix}\n",
    "2 & 1 & 4 & 1 \\\\ \n",
    "3 & 4 & -1 & -1 \\\\ \n",
    "1 & -4 & 1 & 5 \\\\ \n",
    "2 & -2 & 1 & 3 \\\\ \n",
    "\\end{pmatrix}\n",
    "\\begin{pmatrix} \n",
    "w \\\\ \n",
    "x \\\\ \n",
    "y \\\\ \n",
    "z \\\\ \n",
    "\\end{pmatrix}\n",
    "=\n",
    "\\begin{pmatrix}\n",
    "-4 \\\\\n",
    "3 \\\\\n",
    "9 \\\\\n",
    "7 \\\\\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "We can use `NumPy` arrays to represent the matrix and the vector of parameter values, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-11-11T14:39:32.310258Z",
     "start_time": "2019-11-11T14:39:32.119801Z"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-11-11T14:39:32.318350Z",
     "start_time": "2019-11-11T14:39:32.311479Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 2.  1.  4.  1.]\n",
      " [ 3.  4. -1. -1.]\n",
      " [ 1. -4.  1.  5.]\n",
      " [ 2. -2.  1.  3.]]\n",
      "(4, 4)\n",
      "[-4.  3.  9.  7.]\n",
      "(4,)\n"
     ]
    }
   ],
   "source": [
    "A = np.array([[2., 1., 4., 1.], \n",
    "              [3., 4., -1., -1.], \n",
    "              [1., -4., 1., 5.], \n",
    "              [2., -2., 1., 3.]])\n",
    "print( A ) \n",
    "print( A.shape ) \n",
    "\n",
    "v = np.array([-4., 3., 9., 7.])\n",
    "print( v ) \n",
    "print( v.shape ) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, we could write out the matrices using a shorthand notation, as:\n",
    "\n",
    "$$\n",
    "\\mathbf{A} \\mathbf{x} = \\mathbf{v}\n",
    "$$\n",
    "\n",
    "where $\\mathbf{x} = (w, x, y, z)$ and the matrix $\\mathbf{A}$ and vector $\\mathbf{v}$ take the appropriate values.\n",
    "\n",
    "One way to solve equations of this form is to find the inverse of the matrix $\\mathbf{A}$ then multiply both sides to get the\n",
    "solution $\\mathbf{x} = \\mathbf{A}^{-1} \\mathbf{v}$.  This sounds like a promising approach for solving equations on the\n",
    "computer, but in practice it is not so good.  Inverting the matrix $\\mathbf{A}$ is a rather complicated calculation that\n",
    "is inefficient and cumebrsome to carry out numerically.  There are other ways of solving simultaneous equations that don't\n",
    "require us to calculate an inverse and it turns out that these are faster, simpler, and more accurate.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Gaussian Elimination\n",
    "\n",
    "\n",
    "You may have practiced the following procedures/rules before.  Suppose we wish to solve a set of simultaneous linear equations\n",
    "like the one we have just discussed.  We will carry out the solution by working on the matrix form \n",
    "$\\mathbf{A} \\mathbf{x} = \\mathbf{v}$ of the equations.  The following useful rules apply:\n",
    "\n",
    "1. We can multiply any of our simultaneous equations by a constant and its still the same equation.  For instance we can\n",
    "multiply the first equation/row by 2 to get $4w + 2x + 8y + 2z = -8$ and the solution for $w$, $x$, $y$, $z$\n",
    "stays the same.  To put that another way: *If we multiply any row of the matrix $\\mathbf{A}$ by any constant, and\n",
    "we multiply the corresponding row of vector $\\mathbf{v}$ by the same constant, then the solution does not change.*\n",
    "1. We can take any linear combination of two equations to get another correct equation.  To put that another way:\n",
    "*If we add to or subtract from any row of $\\mathbf{A}$ a multiple of any other row, and we do the same for\n",
    "$\\mathbf{v}$, then the solution does not change.*\n",
    "\n",
    "We can use these two basic operations to create the Gaussian Elimination algorithm.  Consider our previous\n",
    "set of simultaneous equations and let us perform the following steps:\n",
    "\n",
    "1. We divide the first row by the top-left element of the matrix, which has the value of 2 in this case.  Recall\n",
    "that we must divide both the matrix itself and the corresponding element on the right-hand side of the equation,\n",
    "in order that the equations remain correct.  Thus we get:\n",
    "$$\n",
    "\\begin{pmatrix}\n",
    "1 & 0.5 & 2 & 0.5 \\\\ \n",
    "3 & 4 & -1 & -1 \\\\ \n",
    "1 & -4 & 1 & 5 \\\\ \n",
    "2 & -2 & 1 & 3 \\\\ \n",
    "\\end{pmatrix}\n",
    "\\begin{pmatrix} \n",
    "w \\\\ \n",
    "x \\\\ \n",
    "y \\\\ \n",
    "z \\\\ \n",
    "\\end{pmatrix}\n",
    "=\n",
    "\\begin{pmatrix}\n",
    "-2 \\\\\n",
    "3 \\\\\n",
    "9 \\\\\n",
    "7 \\\\\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "Because we have divided both on the left and on the right, the solution to the equations is unchanged from before, but note\n",
    "that the top-left element of the matrix, by definition, is now equal to 1.\n",
    "2. Next, note that the first element in the second row of the matrix is 3.  If we now subtract 3 times the first\n",
    "row from the second this element will become zero, thus:\n",
    "$$\n",
    "\\begin{pmatrix}\n",
    "1 & 0.5 & 2 & 0.5 \\\\ \n",
    "0 & 2.5 & -7 & -2.5 \\\\ \n",
    "1 & -4 & 1 & 5 \\\\ \n",
    "2 & -2 & 1 & 3 \\\\ \n",
    "\\end{pmatrix}\n",
    "\\begin{pmatrix} \n",
    "w \\\\ \n",
    "x \\\\ \n",
    "y \\\\ \n",
    "z \\\\ \n",
    "\\end{pmatrix}\n",
    "=\n",
    "\\begin{pmatrix}\n",
    "-2 \\\\\n",
    "9 \\\\\n",
    "9 \\\\\n",
    "7 \\\\\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "Notice again that we have performed the same subtraction on the right-hand side of the equation, to make sure the solution\n",
    "remains unchanged.\n",
    "3. We now do a similar trick with the third and fourth rows.  These have first elements equal to 1 and 2 respectively, so\n",
    "we subtract 1 times the first row from the third, and 2 times the first row from the fourth, which give us the\n",
    "following:\n",
    "$$\n",
    "\\begin{pmatrix}\n",
    "1 & 0.5 & 2 & 0.5 \\\\ \n",
    "0 & 2.5 & -7 & -2.5 \\\\ \n",
    "0 & -4.5 & -1 & 4.5 \\\\ \n",
    "0 & -3 & -3 & 2 \\\\ \n",
    "\\end{pmatrix}\n",
    "\\begin{pmatrix} \n",
    "w \\\\ \n",
    "x \\\\ \n",
    "y \\\\ \n",
    "z \\\\ \n",
    "\\end{pmatrix}\n",
    "=\n",
    "\\begin{pmatrix}\n",
    "-2 \\\\\n",
    "9 \\\\\n",
    "11 \\\\\n",
    "11 \\\\\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "The end result of this series of operations is that the first column of our matrix has been reduced to the simple form\n",
    "$(1, 0, 0, 0)$, but the solution of the complete set of equations is unchanged.\n",
    "\n",
    "We can perform the same basic two set of operations on our `A` and `v` `NumPy` arrays.  For example, to divide the\n",
    "first row of matrix `A` and vector `v` by the top-left element, and then perform the elimination steps to get 0's in\n",
    "the first column, we can:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-11-11T14:39:32.397486Z",
     "start_time": "2019-11-11T14:39:32.319672Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 1.   0.5  2.   0.5]\n",
      " [ 3.   4.  -1.  -1. ]\n",
      " [ 1.  -4.   1.   5. ]\n",
      " [ 2.  -2.   1.   3. ]]\n",
      "[-2.  3.  9.  7.]\n"
     ]
    }
   ],
   "source": [
    "# step 1, divide first row by top-left element\n",
    "element = A[0, 0]\n",
    "A[0] = A[0] / element\n",
    "print( A ) \n",
    "v[0] = v[0] / element\n",
    "print( v ) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-11-11T14:39:32.471845Z",
     "start_time": "2019-11-11T14:39:32.399051Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 1.   0.5  2.   0.5]\n",
      " [ 0.   2.5 -7.  -2.5]\n",
      " [ 1.  -4.   1.   5. ]\n",
      " [ 2.  -2.   1.   3. ]]\n",
      "[-2.  9.  9.  7.]\n"
     ]
    }
   ],
   "source": [
    "# step 2, eliminate w variable from row 2, by subtracting 3 times first row from the second\n",
    "element = A[1, 0]\n",
    "A[1] = A[1] - element * A[0] # subtract 3 times first row from second row\n",
    "print( A ) \n",
    "\n",
    "v[1] = v[1] - element * v[0]\n",
    "print( v ) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-11-11T14:39:32.572124Z",
     "start_time": "2019-11-11T14:39:32.474525Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 1.   0.5  2.   0.5]\n",
      " [ 0.   2.5 -7.  -2.5]\n",
      " [ 0.  -4.5 -1.   4.5]\n",
      " [ 0.  -3.  -3.   2. ]]\n",
      "[-2.  9. 11. 11.]\n"
     ]
    }
   ],
   "source": [
    "# step 3, eliminate w variable from rows 3 and 4 using same method\n",
    "element = A[2, 0]\n",
    "A[2] = A[2] - element * A[0]\n",
    "v[2] = v[2] - element * v[0]\n",
    "\n",
    "element = A[3, 0]\n",
    "A[3] = A[3] - element * A[0]\n",
    "v[3] = v[3] - element * v[0]\n",
    "\n",
    "print( A ) \n",
    "print( v ) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we move on to the second row in the matrix and perform a series of operations.  First we divide the second row\n",
    "by its *second* element, to get:\n",
    "$$\n",
    "\\begin{pmatrix}\n",
    "1 & 0.5 & 2 & 0.5 \\\\ \n",
    "0 & 1 & -2.8 & -1 \\\\ \n",
    "0 & -4.5 & -1 & 4.5 \\\\ \n",
    "0 & -3 & -3 & 2 \\\\ \n",
    "\\end{pmatrix}\n",
    "\\begin{pmatrix} \n",
    "w \\\\ \n",
    "x \\\\ \n",
    "y \\\\ \n",
    "z \\\\ \n",
    "\\end{pmatrix}\n",
    "=\n",
    "\\begin{pmatrix}\n",
    "-2 \\\\\n",
    "3.6 \\\\\n",
    "11 \\\\\n",
    "11 \\\\\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "Then we subtract the appropriate multiple of the second row from each of the rows below it, so as to make the second element\n",
    "of each of those rows zero.  That is, we subtract $-4.5$ times the second from from the third, and $-3$ times the\n",
    "second row from the fourth, to give:\n",
    "$$\n",
    "\\begin{pmatrix}\n",
    "1 & 0.5 & 2 & 0.5 \\\\ \n",
    "0 & 1 & -2.8 & -1 \\\\ \n",
    "0 & 0 & -13.6 & 0 \\\\ \n",
    "0 & 0 & -11.4 & -1 \\\\ \n",
    "\\end{pmatrix}\n",
    "\\begin{pmatrix} \n",
    "w \\\\ \n",
    "x \\\\ \n",
    "y \\\\ \n",
    "z \\\\ \n",
    "\\end{pmatrix}\n",
    "=\n",
    "\\begin{pmatrix}\n",
    "-2 \\\\\n",
    "3.6 \\\\\n",
    "27.2 \\\\\n",
    "21.8 \\\\\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "Then we move on to the third and fourth rows, and do the same thing, dividing each then subtracting from the rows below\n",
    "(except that the fourth row obviously doesn't have any rows below, so it only needs to be divided).  The end result\n",
    "of the entire set of operations (the Gaussian Elimination algorithm) is the following:\n",
    "$$\n",
    "\\begin{pmatrix}\n",
    "1 & 0.5 & 2 & 0.5 \\\\ \n",
    "0 & 1 & -2.8 & -1 \\\\ \n",
    "0 & 0 & 1 & 0 \\\\ \n",
    "0 & 0 & 0 & 1 \\\\ \n",
    "\\end{pmatrix}\n",
    "\\begin{pmatrix} \n",
    "w \\\\ \n",
    "x \\\\ \n",
    "y \\\\ \n",
    "z \\\\ \n",
    "\\end{pmatrix}\n",
    "=\n",
    "\\begin{pmatrix}\n",
    "-2 \\\\\n",
    "3.6 \\\\\n",
    "-2 \\\\\n",
    "1 \\\\\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "By definition this set of equations still has the same solution for the variables $w$, $x$, $y$ and $z$ as the equations\n",
    "we started with, but the matrix is now *upper triangular*: all the elements below the diagonal are zero.  This lets us \n",
    "know that the solution for $z$ is $z = 1$.  And this allows us to determine the solution for the other variables\n",
    "quite simply by the process of *backsubstitution*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Backsubstitution\n",
    "\n",
    "\n",
    "Suppose we have any set of equations in upper triangular form.  Generically we can represent this as:\n",
    "\n",
    "$$\n",
    "\\begin{pmatrix}\n",
    "1 & a_{01} & a_{02} & a_{03} \\\\ \n",
    "0 & 1 & a_{12} & a_{13} \\\\ \n",
    "0 & 0 & 1 & a_{23} \\\\ \n",
    "0 & 0 & 0 & 1 \\\\ \n",
    "\\end{pmatrix}\n",
    "\\begin{pmatrix} \n",
    "w \\\\ \n",
    "x \\\\ \n",
    "y \\\\ \n",
    "z \\\\ \n",
    "\\end{pmatrix}\n",
    "=\n",
    "\\begin{pmatrix}\n",
    "v_0 \\\\\n",
    "v_1 \\\\\n",
    "v_2 \\\\\n",
    "v_3 \\\\\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "We can write out the equations in full as:\n",
    "\n",
    "$$\n",
    "\\begin{eqnarray}\n",
    "w + a_{01}x + a_{02}y + a_{03}z  &=& v_0 \\\\\n",
    "          x + a_{12} y + a_{13}z &=& v_1 \\\\\n",
    "                     y + a_{23}z &=& v_2 \\\\\n",
    "                               z &=& v_3\n",
    "\\end{eqnarray}\n",
    "$$\n",
    "\n",
    "Note that we are using Python-style numbering for the elements here, starting from zero rather than one (as is more normal\n",
    "in mathematics).  This will make the equations more directly translatable to Python code.\n",
    "\n",
    "Given equations of this form, we see now that the solution for the value of $z$ is trival, it is given directly:\n",
    "\n",
    "$$\n",
    "z = v_3\n",
    "$$\n",
    "\n",
    "But, given this value, the solution for $y$ is also trivial, being given by:\n",
    "\n",
    "$$\n",
    "y = v_2 - a_{23} z = v_2 - a_{23} v_3\n",
    "$$\n",
    "\n",
    "And we can go on, the solution for x and w are\n",
    "\n",
    "$$\n",
    "x = v_1 - a_{12} y - a_{13} z \\\\\n",
    "w = v_0 - a_{01} x - a_{02} y - a_{03} z\n",
    "$$\n",
    "\n",
    "Applying these formulas (known as backsubstitution) to our previous concrete upper triangle form equation after doing\n",
    "elimination, yields the final result:\n",
    "\n",
    "$$\n",
    "w = 2, \\; x = -1, \\; y = -2, \\; z = 1\n",
    "$$\n",
    "\n",
    "Thus by the combination of Gaussian elimination with backsubstitution we have solved our set of simultaneous equations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Algorithm for Gaussian Elimination with Backsubstitution\n",
    "\n",
    "\n",
    "We are now in a position to create a complete program for solving simultaneous equations.  Here is a program to solve the \n",
    "equations using Gaussian elimination and backsubstitution:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-11-11T14:39:32.664495Z",
     "start_time": "2019-11-11T14:39:32.574299Z"
    }
   },
   "outputs": [],
   "source": [
    "def gaussian_elimination(A, v):\n",
    "    \"\"\"Given a numpy square array A, representing the parameters of a system of linear equations, and the \n",
    "    parameter vector v for the system of equations Ax = v, this function performs Gaussian elimination\n",
    "    on the A matrix and v vector, and returns the A matrix in upper triangular form (suitable for solving\n",
    "    using backsubstitution.\n",
    "    \n",
    "    Paramters\n",
    "    ---------\n",
    "    A -- A square numpy array, representing parameters of system of linear equations\n",
    "    v -- A (N,) sized numpy array vector, representing the solution vector of a system of linear equations\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    A,v -- Returns A in upper triangular form, and the corresponding new set of v values after gaussian elimination\n",
    "    \"\"\"\n",
    "    N = len(v) # A should be shaped (N,N), and v is a (N,) vector\n",
    "    A_new = np.copy(A) # make copies, so our elimination doesn't effect original\n",
    "    v_new = np.copy(v)\n",
    "\n",
    "    for m in range(N):\n",
    "        # divide by the diagonal element to normalize\n",
    "        div = A_new[m, m]\n",
    "        A_new[m,:] /= div\n",
    "        v_new[m] /= div\n",
    "        \n",
    "        # now subtract from the lower rows\n",
    "        for i in range(m+1, N):\n",
    "            mult = A_new[i, m]\n",
    "            A_new[i,:] -= mult * A_new[m,:]\n",
    "            v_new[i] -= mult * v_new[m]\n",
    "            \n",
    "    return A_new,v_new\n",
    "\n",
    "\n",
    "def backsubstitution(A, v):\n",
    "    \"\"\"Given a numpy square array A, representing the upper triangular transformed\n",
    "    parameters of a system of linear equations, and the  parameter vector v for the system of equations \n",
    "    Ax = v, this function performs backsubstitution in order to find the values that solve\n",
    "    the system of linear equations\n",
    "    \n",
    "    Paramters\n",
    "    ---------\n",
    "    A -- A square numpy array, in upper triangular form, representing parameters of system of linear equations\n",
    "    v -- A (N,) sized numpy array vector, representing the solution vector of a system of linear equations\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    x -- A (N,) sized numpy array vector representing the solved values for the AX = v system\n",
    "    \"\"\"\n",
    "    N = len(v) # A should be shaped (N,N), and v is (N,) vector\n",
    "    x = np.empty(N, float) # holds results we will return\n",
    "    \n",
    "    for m in range(N-1, -1, -1):\n",
    "        x[m] = v[m]\n",
    "        for i in range(m+1, N):\n",
    "            x[m] -= A[m, i] * x[i]\n",
    "    return x\n",
    "\n",
    "def linear_system_solve(A, v):\n",
    "    \"\"\"Given a numpy square array A, representing a system of linear equations, and the corresponding\n",
    "    vector v of solution parameters, for the system Ax = v, solve the system of equations, using \n",
    "    Gaussian elimination and Backsubstitution.\n",
    "\n",
    "    Paramters\n",
    "    ---------\n",
    "    A -- A square numpy array, representing parameters of system of linear equations\n",
    "    v -- A (N,) sized numpy array vector, representing the solution vector of a system of linear equations\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    x -- A (N,) sized numpy array vector representing the solved values for the AX = v system\n",
    "    \"\"\"\n",
    "    A_upper, v_upper = gaussian_elimination(A, v)\n",
    "    x = backsubstitution(A_upper, v_upper)\n",
    "    return x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-11-11T14:39:32.766694Z",
     "start_time": "2019-11-11T14:39:32.669496Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ 2. -1. -2.  1.]\n"
     ]
    }
   ],
   "source": [
    "# example, solve the same system we used as an example above\n",
    "A = np.array([[2., 1., 4., 1.], \n",
    "              [3., 4., -1., -1.], \n",
    "              [1., -4., 1., 5.], \n",
    "              [2., -2., 1., 3.]])\n",
    "\n",
    "v = np.array([-4., 3., 9., 7.])\n",
    "\n",
    "x = linear_system_solve(A, v)\n",
    "print( x ) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a number of feaatures to note about this program.  We store the matrices and vectors as `NumPy` arrays, whose\n",
    "initial values are set by the user for the system they wish to solve.  The elimination portion of the\n",
    "program goes through each row of the matrix, one by one, and first normalizes it by dividing by the appropriate diagonal\n",
    "element, then subtracts a multiple of that row from each lower row.  Notice how the program uses Python's ability to\n",
    "perform operations on an entire row at once, which makes the calculation faster and simpler to program.  The\n",
    "second function is a straightforward version of the backsubstitution procedure.  The functions should work for matrices\n",
    "of any size, as we query the size of the matrices (`N`) and use this parameter when doing the normalization, subtraction\n",
    "and backsubstitution loops.\n",
    "\n",
    "The `NumPy` linear algebra library has an equivalent method, the `solve` method, that can be used to perform this\n",
    "exact same procedure, to find the solution to a system of simultaneous linear equations:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-11-11T14:39:32.839236Z",
     "start_time": "2019-11-11T14:39:32.770263Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ 2. -1. -2.  1.]\n"
     ]
    }
   ],
   "source": [
    "x2 = np.linalg.solve(A, v)\n",
    "print( x2 ) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Pivoting\n",
    "\n",
    "\n",
    "Suppose the equations we want to solve are slightly different from those of the previous section, thus:\n",
    "\n",
    "$$\n",
    "\\begin{pmatrix}\n",
    "0 & 1 & 4 & 1 \\\\ \n",
    "3 & 4 & -1 & -1 \\\\ \n",
    "1 & -4 & 1 & 5 \\\\ \n",
    "2 & -2 & 1 & 3 \\\\ \n",
    "\\end{pmatrix}\n",
    "\\begin{pmatrix} \n",
    "w \\\\ \n",
    "x \\\\ \n",
    "y \\\\ \n",
    "z \\\\ \n",
    "\\end{pmatrix}\n",
    "=\n",
    "\\begin{pmatrix}\n",
    "-4 \\\\\n",
    "3 \\\\\n",
    "9 \\\\\n",
    "7 \\\\\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "Just one thing has changed from the old equations, the first element of the first row of the matrix is zero, where previously\n",
    "it was nonzero.  But this makes all the difference in the world, because the first step of our Gaussian elimination procedure\n",
    "requires us to divide the first row of the matrix by its first element, which we can no longer do, because we would have to \n",
    "divide by zero.  In cases like these, Gaussian elimination no longer works.  So what are we to do?\n",
    "\n",
    "The standard solution is to use *pivoting*, which means simply interchanging the rows of the matrix to get rid of the problem. \n",
    "Clearly we are allowed to interchange the order in which we write our equations, it will not affect their solution.  So we\n",
    "could swap the first and second equations:\n",
    "\n",
    "$$\n",
    "\\begin{pmatrix}\n",
    "3 & 4 & -1 & -1 \\\\ \n",
    "0 & 1 & 4 & 1 \\\\ \n",
    "1 & -4 & 1 & 5 \\\\ \n",
    "2 & -2 & 1 & 3 \\\\ \n",
    "\\end{pmatrix}\n",
    "\\begin{pmatrix} \n",
    "w \\\\ \n",
    "x \\\\ \n",
    "y \\\\ \n",
    "z \\\\ \n",
    "\\end{pmatrix}\n",
    "=\n",
    "\\begin{pmatrix}\n",
    "3 \\\\\n",
    "-4 \\\\\n",
    "9 \\\\\n",
    "7 \\\\\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "Notice we have swapped the first and second rows of the $A$ matrix, as well as the first and second row of the $v$ vector\n",
    "as well.  Now the first element of the matrix is no longer zero, and Gaussian elimination will work just fine.\n",
    "\n",
    "This problem can occur any time in the elimination process.  Thus we need a general method to try and pivot such problems\n",
    "away.  A good general scheme that works well in most cases is the so-called *partial pivoting* method, which is as\n",
    "follows.  As we have seen, Gaussian elimination works down the rows of the matrix one by one, dividing each by the\n",
    "appropriate diagonal element before performing subtractions.  With partial pivoting we consider rearranging the\n",
    "rows at each stage, before doing our division.  We look for the largest value in the current column being pivoted \n",
    "(e.g. the one furtherst from zero).  If the row containing this value is not the current row, we swap this row\n",
    "with the current row, before proceeding on to do the division normalization.  This has the result of ensuring that the element\n",
    "we divide by in our Gaussian elimination is always as far from zero as possible.\n",
    "\n",
    "In practice, though we didn't implement it in our previous version, one should always use pivoting when applying\n",
    "Gaussian elimination, since you rarely known in advance when the equations you are trying to solve will present a problem."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Versions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-11-11T14:39:33.036963Z",
     "start_time": "2019-11-11T14:39:32.841186Z"
    }
   },
   "outputs": [
    {
     "data": {
      "application/json": {
       "Software versions": [
        {
         "module": "Python",
         "version": "3.7.4 64bit [GCC 7.3.0]"
        },
        {
         "module": "IPython",
         "version": "7.8.0"
        },
        {
         "module": "OS",
         "version": "Linux 4.18.0 17 generic x86_64 with debian buster sid"
        },
        {
         "module": "numpy",
         "version": "1.16.5"
        },
        {
         "module": "scipy",
         "version": "1.3.1"
        },
        {
         "module": "matplotlib",
         "version": "3.1.1"
        }
       ]
      },
      "text/html": [
       "<table><tr><th>Software</th><th>Version</th></tr><tr><td>Python</td><td>3.7.4 64bit [GCC 7.3.0]</td></tr><tr><td>IPython</td><td>7.8.0</td></tr><tr><td>OS</td><td>Linux 4.18.0 17 generic x86_64 with debian buster sid</td></tr><tr><td>numpy</td><td>1.16.5</td></tr><tr><td>scipy</td><td>1.3.1</td></tr><tr><td>matplotlib</td><td>3.1.1</td></tr><tr><td colspan='2'>Mon Nov 11 08:39:33 2019 CST</td></tr></table>"
      ],
      "text/latex": [
       "\\begin{tabular}{|l|l|}\\hline\n",
       "{\\bf Software} & {\\bf Version} \\\\ \\hline\\hline\n",
       "Python & 3.7.4 64bit [GCC 7.3.0] \\\\ \\hline\n",
       "IPython & 7.8.0 \\\\ \\hline\n",
       "OS & Linux 4.18.0 17 generic x86\\_64 with debian buster sid \\\\ \\hline\n",
       "numpy & 1.16.5 \\\\ \\hline\n",
       "scipy & 1.3.1 \\\\ \\hline\n",
       "matplotlib & 3.1.1 \\\\ \\hline\n",
       "\\hline \\multicolumn{2}{|l|}{Mon Nov 11 08:39:33 2019 CST} \\\\ \\hline\n",
       "\\end{tabular}\n"
      ],
      "text/plain": [
       "Software versions\n",
       "Python 3.7.4 64bit [GCC 7.3.0]\n",
       "IPython 7.8.0\n",
       "OS Linux 4.18.0 17 generic x86_64 with debian buster sid\n",
       "numpy 1.16.5\n",
       "scipy 1.3.1\n",
       "matplotlib 3.1.1\n",
       "Mon Nov 11 08:39:33 2019 CST"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%load_ext version_information\n",
    "\n",
    "%version_information numpy, scipy, matplotlib"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Acknowledgement\n",
    "----------------\n",
    "\n",
    "The content of this notebook are based upon and borrowed heavily from Newman \"Computational Physics\" Chapter 6.1."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
